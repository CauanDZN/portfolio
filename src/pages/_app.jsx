import '../styles/globals.css'
import Header from '../components/Header'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Cauan.dev</title>
        <link rel="shortcut icon" href="/images/logo.png" />
      </Head>
       <Header />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
