import React from 'react'
import { Container, Content } from '../styles/contact'
import { FaLinkedin, FaFacebook, FaInstagram, FaEnvelope, FaGithub } from 'react-icons/fa'
import ItemContact from '../components/ItemContact'

export default function Contact() {
  return (
    <Container>
      <Content>
        <ItemContact 
          IconFa={FaLinkedin} 
          LinkContact={<a href="https://www.linkedin.com/in/francisco-cauan-victor/" target="_blank">https://www.linkedin.com/in/francisco-cauan-victor/</a>}
        />
        <ItemContact 
          IconFa={FaGithub} 
          LinkContact={<a href="https://github.com/CauanDZN" target="_blank">https://github.com/CauanDZN</a>} 
        />
        <ItemContact 
          IconFa={FaInstagram} 
          LinkContact={<a href="https://www.instagram.com/cauanvictoroficial/" target="_blank">https://www.instagram.com/cauanvictoroficial/</a>}
        />
        <ItemContact 
          IconFa={FaEnvelope} 
          LinkContact={<a href="mailto:cauanvictor0325@gmail.com">cauanvictor0325@gmail.com</a>} 
        />
      </Content>
    </Container>
  )
}
