import { FaArrowRight } from 'react-icons/fa'
import { FiDownload } from 'react-icons/fi'
import { Container, Content, Infos, Name, Function, Intro, Links, Logo, Img } from '../styles/indexStyle'
import Link from 'next/link'

export default function Home() {
  return (
   <Container>
     <Content>
      <Infos>
        <Name>Olá, sou Cauan Victor</Name>
        <Function>Fullstack Developer</Function>
        <Intro>
          Cursando em Redes de Computadores, 
          tenho experiência em desenvolvimento Web e Mobile
          (React, JS, TS, Node, Express, etc).
        </Intro>
        <Link href="projects">
          <Links>PROJETOS <FaArrowRight /></Links>
        </Link>
        <Link href="/pdf/CV - Francisco Cauan Victor - Portuguese.pdf" download>
          <Links>DOWNLOAD CV <FiDownload /></Links>
        </Link>
      </Infos>
      <Logo>
        <Img src="https://github.com/CauanDZN.png" alt="logo" />
      </Logo>
     </Content>
   </Container>
  )
}
