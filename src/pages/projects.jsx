import React from 'react'
import { Container, Content, Ul, Li, TitleProject, DescriptionProject, Img } from '../styles/projects'

export default function Projects() {
  return (
    <Container>
     <Content>
       <Ul>
       <Li href="https://dashgo-cauandzn.vercel.app/dashboard" target="_blank">
            <Img src="https://i.imgur.com/4MHjVUA.png" title="source: imgur.com" />
            <TitleProject>
              Dashgo
            </TitleProject>
            <DescriptionProject>
              NextJS - TypeScript - Chakra-UI
            </DescriptionProject>
        </Li>

        <Li href="https://cauan-todo-react.vercel.app/" target="_blank">
           <Img src="https://i.imgur.com/KBOALmz.png" title="source: imgur.com" />
            <TitleProject>
              To Do List
            </TitleProject>
            <DescriptionProject>
              ReactJS - TypeScript - Styled-components
            </DescriptionProject>
        </Li>


        <Li href="https://event-platform-cauandzn.vercel.app/" target="_blank">
            <Img src="https://i.imgur.com/8ifSgeS.png" title="source: imgur.com" />
            <TitleProject>
              Plataforma de Eventos
            </TitleProject>
            <DescriptionProject>
              ReactJS - TypeScript
            </DescriptionProject>
        </Li>

        <Li href="https://consulta-cep-cauan.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/0qVqvHY.png" title="source: imgur.com" />
            <TitleProject>
              Consulta CEP
            </TitleProject>
            <DescriptionProject>
              ReactJS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://site-do-cauan.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/ruBNMuW.png" title="source: imgur.com" />
            <TitleProject>
              Site do Cauan
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://doctorcare-cauan.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/5OFnfqM.png" title="source: imgur.com" />
            <TitleProject>
              Doctorcare
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://pokedex-cauandzn.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/uEAysF4.png" title="source: imgur.com" />
            <TitleProject>
              Pokedex
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://calculadora-online.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/6wJnZOr.png" title="source: imgur.com" />
            <TitleProject>
              Calculadora Online
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://netflix-clone-cauandzn.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/tygUjMR.png" title="source: imgur.com" />
            <TitleProject>
              Cauanflix
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>

        <Li href="https://eeep-lmb.vercel.app/" target="_blank">
        <Img src="https://i.imgur.com/C53XsKy.png" title="source: imgur.com" />
            <TitleProject>
              Site da EEEP Leonel de Moura Brizola
            </TitleProject>
            <DescriptionProject>
              HTML - CSS - JavaScript
            </DescriptionProject>
        </Li>
       </Ul>
     </Content>
   </Container>
  )
}